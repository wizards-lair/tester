# (cons 'scheme 'testing)

This is an implementation of a simple testing mechanism for R7RS Scheme, it's based on the lightweight unit test mechanism for R5RS Scheme named Testeez by Neil Van Dyke.

This library exports the special form `tester` which preserves the same syntax and semantics from Van Dykes code but differs in implementation a little bit.

## tester specification
```
(tester test-title
  test1
  ...
  testN)
```

A series of tester tests is listed within the `tester` syntax, each test can be of the following forms:

```
(test/equal description expression expected)
```

Execute a test case. description is a short title or description of the test case, expression is a Scheme expression, and expected is an expression for the expected value (or multiple values). The test case passes if and only if each value of expr is equal? to the corresponding value of expected.

```
(expression expected)
```

Shorthand for (test/equal "" expression expected). 

```
(test/eq description expression expected)
```

Like test/equal, except the equivalence predicate is eq? rather than equal?.

```
(test/eqv description expression expected)
```

Like test/equal, except the equivalence predicate is eqv? rather than equal?.

```
(test-define description identifier value)
```

Bind a variable. description is a short description string, identifier is the identifier to be bounded, and value is the value expression. The binding is visible to the remainder of the enclosing tester syntax.
